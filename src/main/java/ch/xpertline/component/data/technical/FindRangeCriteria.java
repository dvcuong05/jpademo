package ch.xpertline.component.data.technical;

import java.io.Serializable;

/**
 * FindRangeCriteria defines the attributes to restrict the result for
 * pagination
 *
 * @author tifxb
 */
public class FindRangeCriteria implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Used to set the initial first results offsets that begins at 0
     */
    private Integer from;
    /**
     * Used to set the max results (from-to)
     */
    private Integer to;

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }
}
