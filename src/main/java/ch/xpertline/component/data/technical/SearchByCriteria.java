package ch.xpertline.component.data.technical;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * SearchByCriteria defines lists of searchCriteria objects for a search 
 * By default each searchCriteria object defined on the criteria list 
 * is applied with a select where "and" logical operator. 
 *
 * @author tifxb
 */
public class SearchByCriteria implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<SearchCriteria> criteria = null;
    // Suport only OR, AND was removed to simplify the search criterias
    private List<SearchByCriteria> or = null;

    public List<SearchCriteria> getCriteria() {
        return criteria;
    }

    public void setCriteria(List<SearchCriteria> criteria) {
        this.criteria = criteria;
    }
    
    public void addCriteria(SearchCriteria criteria) {
        if(this.criteria == null){
            this.criteria = new ArrayList<>();
        }
        this.criteria.add(criteria);
    }

    public List<SearchByCriteria> getOr() {
        return or;
    }

    public void setOr(List<SearchByCriteria> or) {
        this.or = or;
    }
    
    public void addOr(SearchByCriteria or) {
        if(this.or == null){
            this.or = new ArrayList<>();
        }
        this.or.add(or);
    }
}
