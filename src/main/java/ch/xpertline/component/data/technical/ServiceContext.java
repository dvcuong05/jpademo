package ch.xpertline.component.data.technical;

import java.io.Serializable;

/**
 * This class contains the standard required context information to provide as
 * parameter when calling a method of a service.
 */
public class ServiceContext implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * The user who is allowed to access to the data
     */
    private String userId;

    /**
     * The environment name that defines the datatasource. If nothing is passed,
     * Default is used
     */
    private String environmentName;

    /**
     * Regional settings like (fr_CH). If nothing is passed, the server settings
     * are used.
     */
    private String regionalSettings;

    /**
     * This parameter will allow to override the rights associated to the user
     * in some context, for instance during the execution of a process for a
     * specific task
     */
    private String contextualRole;

    /**
     * Gets the field userId.
     *
     * @return the value of the field userId; may be null.
     */
    public String getUserId() {
        return this.userId;
    }

    /**
     * Sets the field userId.
     *
     * @param userId the new value of the field userId.
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * Gets the field environmentName.
     *
     * @return the value of the field environmentName; may be null.
     */
    public String getEnvironmentName() {
        return this.environmentName;
    }

    /**
     * Sets the field environmentName.
     *
     * @param environmentName the new value of the field environmentName.
     */
    public void setEnvironmentName(String environmentName) {
        this.environmentName = environmentName;
    }

    /**
     * Gets the field regionalSettings.
     *
     * @return the value of the field regionalSettings; may be null.
     */
    public String getRegionalSettings() {
        return this.regionalSettings;
    }

    /**
     * Sets the field regionalSettings.
     *
     * @param regionalSettings the new value of the field regionalSettings.
     */
    public void setRegionalSettings(String regionalSettings) {
        this.regionalSettings = regionalSettings;
    }

    /**
     * @return the contextualRole
     */
    public String getContextualRole() {
        return contextualRole;
    }

    /**
     * @param contextualRole the contextualRole to set
     */
    public void setContextualRole(String contextualRole) {
        this.contextualRole = contextualRole;
    }

}
