/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.xpertline.j2se.xtao.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hanguyen
 */
@Entity
@Table(name = "SC3RCT")
@XmlRootElement
public class ElementTaxationEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @EmbeddedId
    private ElementTaxationPK id;
    
    @Column(name = "USZA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    
    @Column(name = "US10A")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationTime;
    
    @Column(name = "US11A")
    private String userIdCreation;
    
    @Column(name = "US12A")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModificationDate;
    
    @Column(name = "US13A")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModificationTime;
    
    @Column(name = "US14A")
    private String userIdLastModification;
    
    @Column(name = "SC4TFA")
    private Integer noLigneAffichage;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SC3S2A")
    private BigDecimal montantPeriode1;
    
    @Column(name = "SC3S3A")
    private BigDecimal montantPeriode2;
    
    @Column(name = "SC49MA")
    private BigDecimal montantTaxateur1;
    
    @Column(name = "SC49NA")
    private BigDecimal montantTaxateur2;
    
    @Column(name = "SC3S4A")
    private String elementValide;
    
    @Column(name = "SC233A")
    private String modifieParUneTI;
    
    @Column(name = "SC4TBA")
    private String modifieALImportation;
    
    @Column(name = "SC4TCA")
    private String typeDeModification;
    
    @Column(name = "SC1M7A")
    private String codeGroupeEconomique;

    /**
     * @return the id
     */
    public ElementTaxationPK getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(ElementTaxationPK id) {
        this.id = id;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the creationTime
     */
    public Date getCreationTime() {
        return creationTime;
    }

    /**
     * @param creationTime the creationTime to set
     */
    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    /**
     * @return the userIdCreation
     */
    public String getUserIdCreation() {
        return userIdCreation;
    }

    /**
     * @param userIdCreation the userIdCreation to set
     */
    public void setUserIdCreation(String userIdCreation) {
        this.userIdCreation = userIdCreation;
    }

    /**
     * @return the lastModificationDate
     */
    public Date getLastModificationDate() {
        return lastModificationDate;
    }

    /**
     * @param lastModificationDate the lastModificationDate to set
     */
    public void setLastModificationDate(Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    /**
     * @return the lastModificationTime
     */
    public Date getLastModificationTime() {
        return lastModificationTime;
    }

    /**
     * @param lastModificationTime the lastModificationTime to set
     */
    public void setLastModificationTime(Date lastModificationTime) {
        this.lastModificationTime = lastModificationTime;
    }

    /**
     * @return the userIdLastModification
     */
    public String getUserIdLastModification() {
        return userIdLastModification;
    }

    /**
     * @param userIdLastModification the userIdLastModification to set
     */
    public void setUserIdLastModification(String userIdLastModification) {
        this.userIdLastModification = userIdLastModification;
    }

    /**
     * @return the noLigneAffichage
     */
    public Integer getNoLigneAffichage() {
        return noLigneAffichage;
    }

    /**
     * @param noLigneAffichage the noLigneAffichage to set
     */
    public void setNoLigneAffichage(Integer noLigneAffichage) {
        this.noLigneAffichage = noLigneAffichage;
    }

    /**
     * @return the montantPeriode1
     */
    public BigDecimal getMontantPeriode1() {
        return montantPeriode1;
    }

    /**
     * @param montantPeriode1 the montantPeriode1 to set
     */
    public void setMontantPeriode1(BigDecimal montantPeriode1) {
        this.montantPeriode1 = montantPeriode1;
    }

    /**
     * @return the montantPeriode2
     */
    public BigDecimal getMontantPeriode2() {
        return montantPeriode2;
    }

    /**
     * @param montantPeriode2 the montantPeriode2 to set
     */
    public void setMontantPeriode2(BigDecimal montantPeriode2) {
        this.montantPeriode2 = montantPeriode2;
    }

    /**
     * @return the montantTaxateur1
     */
    public BigDecimal getMontantTaxateur1() {
        return montantTaxateur1;
    }

    /**
     * @param montantTaxateur1 the montantTaxateur1 to set
     */
    public void setMontantTaxateur1(BigDecimal montantTaxateur1) {
        this.montantTaxateur1 = montantTaxateur1;
    }

    /**
     * @return the montantTaxateur2
     */
    public BigDecimal getMontantTaxateur2() {
        return montantTaxateur2;
    }

    /**
     * @param montantTaxateur2 the montantTaxateur2 to set
     */
    public void setMontantTaxateur2(BigDecimal montantTaxateur2) {
        this.montantTaxateur2 = montantTaxateur2;
    }

    /**
     * @return the elementValide
     */
    public String getElementValide() {
        return elementValide;
    }

    /**
     * @param elementValide the elementValide to set
     */
    public void setElementValide(String elementValide) {
        this.elementValide = elementValide;
    }

    /**
     * @return the modifieParUneTI
     */
    public String getModifieParUneTI() {
        return modifieParUneTI;
    }

    /**
     * @param modifieParUneTI the modifieParUneTI to set
     */
    public void setModifieParUneTI(String modifieParUneTI) {
        this.modifieParUneTI = modifieParUneTI;
    }

    /**
     * @return the modifieALImportation
     */
    public String getModifieALImportation() {
        return modifieALImportation;
    }

    /**
     * @param modifieALImportation the modifieALImportation to set
     */
    public void setModifieALImportation(String modifieALImportation) {
        this.modifieALImportation = modifieALImportation;
    }

    /**
     * @return the typeDeModification
     */
    public String getTypeDeModification() {
        return typeDeModification;
    }

    /**
     * @param typeDeModification the typeDeModification to set
     */
    public void setTypeDeModification(String typeDeModification) {
        this.typeDeModification = typeDeModification;
    }

    /**
     * @return the codeGroupeEconomique
     */
    public String getCodeGroupeEconomique() {
        return codeGroupeEconomique;
    }

    /**
     * @param codeGroupeEconomique the codeGroupeEconomique to set
     */
    public void setCodeGroupeEconomique(String codeGroupeEconomique) {
        this.codeGroupeEconomique = codeGroupeEconomique;
    }
 
    
}
