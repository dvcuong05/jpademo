package ch.xpertline.j2se.jpademo;

/**
 * A class to define configuration variables for Http Client
 * @author nhvtuyen
 */
public class HttpContext {

    private HttpContext() {
    }

    private static final int HTTP_CLIENT_CONNECTION_MAX_TOTAL = 200;
    private static final int HTTP_CLIENT_CONNECTION_MAX_PER_ROUTE = 200;
    public static final String HTTP_SSL_CLIENT_TRUST_STORE_FILE = "xline-truststore.jks" ;
    private static final String HTTP_SSL_CLIENT_TRUST_STORE_PASSWORD = "changeit" ;
    private static final String HTTP_SSL_CLIENT_TRUST_STORE_TYPE = "JKS" ;
    private static int httpConnectionMaxTotal = HTTP_CLIENT_CONNECTION_MAX_TOTAL;
    private static int httpConnectionMaxPerRoute = HTTP_CLIENT_CONNECTION_MAX_PER_ROUTE;
    private static String sslClientTrustStoreFile = HTTP_SSL_CLIENT_TRUST_STORE_FILE ;
    private static String sslClientTrustStorePassword = HTTP_SSL_CLIENT_TRUST_STORE_PASSWORD ;
    private static String sslClientTrustStoreType = HTTP_SSL_CLIENT_TRUST_STORE_TYPE ;

    public static int getHttpConnectionMaxTotal() {
        return httpConnectionMaxTotal;
    }

    public static void setHttpConnectionMaxTotal(int httpConnectionMaxTotal) {
        if (httpConnectionMaxTotal < 1) {
            throw new IllegalArgumentException("The HTTP_CLIENT_CONNECTION_MAX_TOTAL parameter should be at least 1");
        }
        HttpContext.httpConnectionMaxTotal = httpConnectionMaxTotal;
    }

    public static int getHttpConnectionMaxPerRoute() {
        return httpConnectionMaxPerRoute;
    }

    public static void setHttpConnectionMaxPerRoute(int httpConnectionMaxPerRoute) {
        if (httpConnectionMaxPerRoute < 1) {
            throw new IllegalArgumentException("The HTTP_CLIENT_CONNECTION_MAX_PER_ROUTE parameter should be at least 1");
        }
        HttpContext.httpConnectionMaxPerRoute = httpConnectionMaxPerRoute;
    }
    
    public static String getSSLClientTrustStoreFile() {
        return sslClientTrustStoreFile ;
    }
    
    public static void setSSLClientTrustStoreFile(String trustStoreFile) {
        HttpContext.sslClientTrustStoreFile = trustStoreFile ;
    }
    
    public static String getSSLClientTrustStorePassword() {
        return sslClientTrustStorePassword ;
    }

    public static void setSSLClientTrustStorePassword(String trustStorePassword) {
        HttpContext.sslClientTrustStorePassword = trustStorePassword ;
    }    
    
    public static String getSSLClientTrustStoreType() {
        return sslClientTrustStoreType ;
    }

    public static void setSSLClientTrustStoreType(String trustStoreType) {
        HttpContext.sslClientTrustStoreType = trustStoreType ;
    }        
}
