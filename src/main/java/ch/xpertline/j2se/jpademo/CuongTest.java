package ch.xpertline.j2se.jpademo;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 *
 * @author dvcuong
 */
public class CuongTest {

    public static void main(String[] args) {
        
        List<String> tests = new ArrayList<>();
        System.out.println("REsult:"+tests.contains("s"));
        
        //proposedDate.add(Calendar.MONTH, documentNature.getExpiryMonths());
        /*EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("sql");
        EntityManager em = emFactory.createEntityManager();
        em.getTransaction().begin();

            //First test case
            findAll(em);
            //System.out.print("TEst:"+toDate("01.01.0001"));
            //String test = "cuong do van lon hon 20 ky tu";
            //System.out.println("REsult:"+test.substring(0,20));
       
        
        //Second test case
        //testQueryWithSetParam(em);
        //testNative(em);
        
       em.getTransaction().commit();
        em.close();
        emFactory.close();*/
        for(int i = 0; i< 0; i++){
            System.out.print(i);
        }
    }
    
    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object,Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
    
    public static String getErrorMessage(Object... params) {
        return MessageFormat.format("Erreur à la création du document  classe de sécurité {0} non définie dans l''environnement {1} .", params);
    }
    
    public static Date toDate(String date) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.parse(date);
    }

    
    public static Object findAll(EntityManager em) {
        //this function only runs once (initialize cache) so use string concatinate instead of StringBuilder for being easier to read.
        String kind = "KINDDOCUMENT";
        String sql = "select * from ecm_fldtyp where typeNme='01'";
        Query query = em.createNativeQuery(sql);
        Object resultList = query.getSingleResult();
        return resultList;
    }
    
     public static boolean checkARAPREPFullTextTriggerExist(EntityManager em){       
        String sql =  "SELECT count(*) FROM sys.objects WHERE [type] = 'TR' AND [name] = 'ARAPREPFullText'";        
        Query statement;
        try {
            statement = em.createNativeQuery(sql);
            statement.getResultList();
            Object count = statement.getSingleResult();            
            return !"0".equalsIgnoreCase(count.toString());
        } catch (Exception ex) {            
            return false;
        }
    }
     
    private static void testQueryWithSetParam(EntityManager em) {
        try {
           StoredProcedureQuery query = em.createStoredProcedureQuery("exportdoc");
        query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(4, String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(5, String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(6, String.class, ParameterMode.IN);
        query.registerStoredProcedureParameter(7, String.class, ParameterMode.OUT);

        query.setParameter(1, "_ALL_");
        query.setParameter(2, "002");
        query.setParameter(3, "Dev002");
        query.setParameter(4, "fr");
        query.setParameter(5, "PROCESSOR");
        query.setParameter(6, "cuongphongtest");

        query.executeUpdate();
            
        } catch (Exception e) {
            System.err.println("testQueryWithSetParam exception:"+ e.getMessage());
            System.exit(0);
        }
    }
    
    private static void testQueryWithoutSetParam(EntityManager em) {
        try {
            Query query;
            String sql = "SELECT id FROM framework_security_custom c WHERE c.ldap-symbol ='xdocadm@dev002'";
            query = em.createNativeQuery(sql);
            List result = query.getResultList();
            System.err.println("testQueryWithoutSetParam result:"+result);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

//    private static void exportDocSQL(EntityManager em) {
//        StoredProcedureQuery query = em.createStoredProcedureQuery("exportdoc");
//        query.registerStoredProcedureParameter("@DOCUMENTID", String.class, ParameterMode.IN);
//        query.registerStoredProcedureParameter("@SEC_ENVIRONMENT", String.class, ParameterMode.IN);
//        query.registerStoredProcedureParameter("@ENVIRONMENT", String.class, ParameterMode.IN);
//        query.registerStoredProcedureParameter("@LANG", String.class, ParameterMode.IN);
//        query.registerStoredProcedureParameter("@ACTION", String.class, ParameterMode.IN);
//        query.registerStoredProcedureParameter("@SESSION_ID", String.class, ParameterMode.IN);
//        query.registerStoredProcedureParameter("result", String.class, ParameterMode.OUT);
//
//        query.setParameter("@SESSION_ID", "cuongdo2");
//        query.setParameter("@DOCUMENTID", "_ALL_");
//        query.setParameter("@SEC_ENVIRONMENT", "J30");
//        query.setParameter("@ENVIRONMENT", "Default");
//        query.setParameter("@LANG", "fr");
//        query.setParameter("@ACTION", "PROCESSOR");
//        query.executeUpdate();
//        System.err.println("******Done");
//    }

//    private static void exportDocAs400(EntityManager em) {
//        StoredProcedureQuery query = em.createStoredProcedureQuery("EXPORTDOC");
//        query.registerStoredProcedureParameter("SESSIONID", String.class, ParameterMode.IN);
//        query.registerStoredProcedureParameter("DOCUMENTID", String.class, ParameterMode.IN);
//        query.registerStoredProcedureParameter("SEC_ENVIRONMENT", String.class, ParameterMode.IN);
//        query.registerStoredProcedureParameter("ENVIRONMENT", String.class, ParameterMode.IN);
//        query.registerStoredProcedureParameter("LANG", String.class, ParameterMode.IN);
//        query.registerStoredProcedureParameter("ACTION", String.class, ParameterMode.IN);
//
//        query.setParameter("SESSIONID", "cuongdo");
//        query.setParameter("DOCUMENTID", "_ALL_");
//        query.setParameter("SEC_ENVIRONMENT", "30A");
//        query.setParameter("ENVIRONMENT", "As400");
//        query.setParameter("LANG", "fr");
//        query.setParameter("ACTION", "PROCESSOR");
//        query.execute();
//    }

   
    private static void testNative(EntityManager em){
        Query query;
        String sql= "SELECT distinct GHD7NU FROM ARGHREP where GHQ1CD=?";
        query = em.createNativeQuery(sql);
       
        query.setParameter(1, "BD001ff");
       List lists =  query.getResultList();
       List<String> result = lists;
       System.err.println(result);
    
    }

    private static class Book {

        private String name;
        private int page;
        public Book(String name, int page) {
            this.name = name;
            this.page = page;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the page
         */
        public int getPage() {
            return page;
        }

        /**
         * @param page the page to set
         */
        public void setPage(int page) {
            this.page = page;
        }
        
    }

}
