package ch.xpertline.j2se.jpademo.entity;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author nvhung
 */
public class ECMParamId implements Serializable{   
     
    private String paramId;    
    private String paramType;

    private ECMParamId() {
    }

    public ECMParamId(String paramId, String paramType) {
        this.paramId = paramId;
        this.paramType = paramType;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ECMParamId other = (ECMParamId) obj;
        if (!Objects.equals(this.paramId, other.paramId)) {
            return false;
        }
        if (!Objects.equals(this.paramType, other.paramType)) {
            return false;
        }
        return true;
    }    
}
