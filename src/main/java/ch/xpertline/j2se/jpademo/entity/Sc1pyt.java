/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.xpertline.j2se.jpademo.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nhvtuyen
 */
@Entity
@Table(name = "SC1PYT")
@XmlRootElement
@NamedStoredProcedureQuery(
            name = "exportdoc", 
            procedureName = "exportdoc", 
            parameters = { 
                    @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "@DOCUMENTID"),
                    @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "@SEC_ENVIRONMENT"), 
                    @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "@ENVIRONMENT"), 
                    @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "@LANG"), 
                    @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "@ACTION"), 
                    @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "@SESSION_ID"), 
                    @StoredProcedureParameter(mode = ParameterMode.OUT, type = String.class, name = "result")
            }
    )
@NamedQueries({    
    @NamedQuery(name = "Sc1pyt.findBySc1qba", query = "SELECT s FROM Sc1pyt s WHERE s.sc1qba = :sc1qba"),
    @NamedQuery(name = "Sc1pyt.findBySc1qza", query = "SELECT s FROM Sc1pyt s WHERE s.sc1qza = :sc1qza"),
    @NamedQuery(name = "Sc1pyt.findBySc1r0a", query = "SELECT s FROM Sc1pyt s WHERE s.sc1r0a = :sc1r0a"),
    @NamedQuery(name = "Sc1pyt.findByUsza", query = "SELECT s FROM Sc1pyt s WHERE s.usza = :usza"),
    @NamedQuery(name = "Sc1pyt.findByUs10a", query = "SELECT s FROM Sc1pyt s WHERE s.us10a = :us10a"),
    @NamedQuery(name = "Sc1pyt.findByUs11a", query = "SELECT s FROM Sc1pyt s WHERE s.us11a = :us11a"),
    @NamedQuery(name = "Sc1pyt.findByUs12a", query = "SELECT s FROM Sc1pyt s WHERE s.us12a = :us12a"),
    @NamedQuery(name = "Sc1pyt.findByUs13a", query = "SELECT s FROM Sc1pyt s WHERE s.us13a = :us13a"),
    @NamedQuery(name = "Sc1pyt.findByUs14a", query = "SELECT s FROM Sc1pyt s WHERE s.us14a = :us14a"),
    @NamedQuery(name = "Sc1pyt.findBySc1lfa", query = "SELECT s FROM Sc1pyt s WHERE s.sc1lfa = :sc1lfa"),
    @NamedQuery(name = "Sc1pyt.findBySc1qda", query = "SELECT s FROM Sc1pyt s WHERE s.sc1qda = :sc1qda"),
    @NamedQuery(name = "Sc1pyt.findBySc5dea", query = "SELECT s FROM Sc1pyt s WHERE s.sc5dea = :sc5dea"),
    @NamedQuery(name = "Sc1pyt.findBySc5dka", query = "SELECT s FROM Sc1pyt s WHERE s.sc5dka = :sc5dka"),
    @NamedQuery(name = "Sc1pyt.findBySc20oa", query = "SELECT s FROM Sc1pyt s WHERE s.sc20oa = :sc20oa"),
    @NamedQuery(name = "Sc1pyt.findBySc20pa", query = "SELECT s FROM Sc1pyt s WHERE s.sc20pa = :sc20pa"),
    @NamedQuery(name = "Sc1pyt.findByUff7a", query = "SELECT s FROM Sc1pyt s WHERE s.uff7a = :uff7a")})
public class Sc1pyt implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "SC1QBA", columnDefinition = "char(10)")
    private String sc1qba;
    @Column(name = "SC1QZA")
    private String sc1qza;
    @Column(name = "SC1R0A")
    private String sc1r0a;
    @Column(name = "USZA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date usza;
    @Column(name = "US10A")
    @Temporal(TemporalType.TIMESTAMP)
    private Date us10a;
    @Column(name = "US11A")
    private String us11a;
    @Column(name = "US12A")
    @Temporal(TemporalType.TIMESTAMP)
    private Date us12a;
    @Column(name = "US13A")
    @Temporal(TemporalType.TIMESTAMP)
    private Date us13a;
    @Column(name = "US14A")
    private String us14a;
    @Column(name = "SC1LFA")
    private Character sc1lfa;
    @Column(name = "SC1QDA")
    private String sc1qda;
    @Column(name = "SC5DEA")
    private Character sc5dea;
    @Column(name = "SC5DKA")
    private Character sc5dka;
    @Column(name = "SC20OA")
    private String sc20oa;
    @Column(name = "SC20PA")
    private Integer sc20pa;
    @Column(name = "UFF7A")
    private String uff7a;

    public Sc1pyt() {
    }

    public Sc1pyt(String sc1qba) {
        this.sc1qba = sc1qba;
    }

    public String getSc1qba() {
        return sc1qba;
    }

    public void setSc1qba(String sc1qba) {
        this.sc1qba = sc1qba;
    }

    public String getSc1qza() {
        return sc1qza;
    }

    public void setSc1qza(String sc1qza) {
        this.sc1qza = sc1qza;
    }

    public String getSc1r0a() {
        return sc1r0a;
    }

    public void setSc1r0a(String sc1r0a) {
        this.sc1r0a = sc1r0a;
    }

    public Date getUsza() {
        return usza;
    }

    public void setUsza(Date usza) {
        this.usza = usza;
    }

    public Date getUs10a() {
        return us10a;
    }

    public void setUs10a(Date us10a) {
        this.us10a = us10a;
    }

    public String getUs11a() {
        return us11a;
    }

    public void setUs11a(String us11a) {
        this.us11a = us11a;
    }

    public Date getUs12a() {
        return us12a;
    }

    public void setUs12a(Date us12a) {
        this.us12a = us12a;
    }

    public Date getUs13a() {
        return us13a;
    }

    public void setUs13a(Date us13a) {
        this.us13a = us13a;
    }

    public String getUs14a() {
        return us14a;
    }

    public void setUs14a(String us14a) {
        this.us14a = us14a;
    }

    public Character getSc1lfa() {
        return sc1lfa;
    }

    public void setSc1lfa(Character sc1lfa) {
        this.sc1lfa = sc1lfa;
    }

    public String getSc1qda() {
        return sc1qda;
    }

    public void setSc1qda(String sc1qda) {
        this.sc1qda = sc1qda;
    }

    public Character getSc5dea() {
        return sc5dea;
    }

    public void setSc5dea(Character sc5dea) {
        this.sc5dea = sc5dea;
    }

    public Character getSc5dka() {
        return sc5dka;
    }

    public void setSc5dka(Character sc5dka) {
        this.sc5dka = sc5dka;
    }

    public String getSc20oa() {
        return sc20oa;
    }

    public void setSc20oa(String sc20oa) {
        this.sc20oa = sc20oa;
    }

    public Integer getSc20pa() {
        return sc20pa;
    }

    public void setSc20pa(Integer sc20pa) {
        this.sc20pa = sc20pa;
    }

    public String getUff7a() {
        return uff7a;
    }

    public void setUff7a(String uff7a) {
        this.uff7a = uff7a;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sc1qba != null ? sc1qba.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sc1pyt)) {
            return false;
        }
        Sc1pyt other = (Sc1pyt) object;
        if ((this.sc1qba == null && other.sc1qba != null) || (this.sc1qba != null && !this.sc1qba.equals(other.sc1qba))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ch.xpertline.j2se.jpademo.entity.Sc1pyt[ sc1qba=" + sc1qba + " ]";
    }
    
}
