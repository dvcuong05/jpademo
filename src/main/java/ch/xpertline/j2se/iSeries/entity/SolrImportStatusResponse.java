package ch.xpertline.j2se.iSeries.entity;

/**
 * Adapter class for SolR status response
 * @author dvcuong
 */
public class SolrImportStatusResponse {
    private String command;
    private String status;
    private Object statusMessages;
    private Object responseHeader;
    private String importResponse;

    /**
     * @return the command
     */
    public String getCommand() {
        return command;
    }

    /**
     * @param command the command to set
     */
    public void setCommand(String command) {
        this.command = command;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the statusMessages
     */
    public Object getStatusMessages() {
        return statusMessages;
    }

    /**
     * @param statusMessages the statusMessages to set
     */
    public void setStatusMessages(Object statusMessages) {
        this.statusMessages = statusMessages;
    }

    /**
     * @return the responseHeader
     */
    public Object getResponseHeader() {
        return responseHeader;
    }

    /**
     * @param responseHeader the responseHeader to set
     */
    public void setResponseHeader(Object responseHeader) {
        this.responseHeader = responseHeader;
    }

    /**
     * @return the importResponse
     */
    public String getImportResponse() {
        return importResponse;
    }

    /**
     * @param importResponse the importResponse to set
     */
    public void setImportResponse(String importResponse) {
        this.importResponse = importResponse;
    }
}
