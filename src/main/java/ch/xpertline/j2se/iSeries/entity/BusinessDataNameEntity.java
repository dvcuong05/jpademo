package ch.xpertline.j2se.iSeries.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The business data entity
 *
 * @author vnbah
 */
@Entity
@Table(name = "framework_security_business_data_name")
@XmlRootElement
public class BusinessDataNameEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer businessDataNameId;
    @Column(name = "languageCode", nullable = true)
    private String languageCode;
    @Column(name = "dataName", nullable = true)
    private String businessDataName;
    
    @ManyToOne
    @JoinColumn(name = "businessDataId", referencedColumnName = "id", nullable = true)
    private BusinessDataEntity businessDataId;
    
    public BusinessDataNameEntity() {
    }
    
    public BusinessDataNameEntity(Integer id) {
        this.businessDataNameId = id;
    }

    public Integer getBusinessDataNameId() {
        return businessDataNameId;
    }

    public void setBusinessDataNameId(Integer businessDataNameId) {
        this.businessDataNameId = businessDataNameId;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getBusinessDataName() {
        return businessDataName;
    }

    public void setBusinessDataName(String businessDataName) {
        this.businessDataName = businessDataName;
    }

    public BusinessDataEntity getBusinessDataId() {
        return businessDataId;
    }

    public void setBusinessDataId(BusinessDataEntity businessDataId) {
        this.businessDataId = businessDataId;
    }
    
}
